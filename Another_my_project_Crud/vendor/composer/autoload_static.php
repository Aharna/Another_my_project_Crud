<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcb5d2c50ac0d1c68bb7b4e4ce3081384
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcb5d2c50ac0d1c68bb7b4e4ce3081384::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcb5d2c50ac0d1c68bb7b4e4ce3081384::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
